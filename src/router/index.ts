import { createRouter, createWebHistory } from 'vue-router'
import EstudianteView from '@/views/EstudianteView.vue'
import EstudiantesView from '@/views/EstudiantesView.vue'
import CreateStudent from '@/components/CreateStudent.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/estudiantes',
      name: 'estudiantes',
      component: EstudiantesView
    },
    {
      path: '/estudiante/:id',
      name: 'estudiante',
      component: EstudianteView
    },
    {
      path: '/create',
      name: 'create',
      component: CreateStudent
    }
  ]
})

export default router
