import { defineStore } from 'pinia';
import StudentService from '@/service/StudentService';
import type IStudent from '@/interface/IStudent';
import { ref } from 'vue';

export const useStudentStore = defineStore('student', () => {
        const students = ref<IStudent[]>([])
        const student = ref<IStudent>()

        async function fetchStudents() {
                try{
                        const service = new StudentService()
                        await service.fetchStudents()
                        students.value = service.getStudents().value
                }catch(error){
                        console.error(error)
                }
        }

        async function fetchStudent(id: number){
                try{
                        const service = new StudentService()
                        await service.fetchStudent(id)
                        student.value = service.getStudent().value
                        console.log("Datos del estudiante en el store:", student.value);
                }catch(error){
                        console.error(error)
                }
        }

        async function createStudent(email: string, name:string, group: string) {
                const service = new StudentService()
                await service.fetchCreate(name, email, group)
                students.value = service.getStudents().value
        }

        async function deleteStudent(id:number){
                const service = new StudentService()
                await service.deleteStudent(id)
                students.value = service.getStudents().value
        }

        return { student,students, fetchStudent, fetchStudents, deleteStudent, createStudent  }
})