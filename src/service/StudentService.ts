import { ref } from 'vue'
import type{ Ref } from 'vue'
import type IStudent from '@/interface/IStudent'
import { useStudentStore } from '@/stores/StudentStore'


const api = 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos'
export default class StudentService{
    private students: Ref<IStudent[]>
    private student: Ref<IStudent>

    constructor(){
        this.students = ref([])
        this.student = ref({}) as Ref<IStudent>
    }

    getStudents(): Ref<IStudent[]>{
        return this.students
    }

    getStudent(): Ref<IStudent>{
        return this.student
    }

    async fetchStudents(): Promise<void>{
        try{
            const resultado = await fetch(api)
            const respuesta = resultado.json()
            this.students.value = await respuesta
        }catch(error){
            console.error(error)
        }
    }

    async fetchStudent(id: number) {
        try{
            const resultado = await fetch (api + '/' + id)
            const respuesta = await resultado.json()
            console.log("Datos del estudiante recibidos:", respuesta);
            this.student.value = respuesta
        }
        catch(error){
            console.error(error)
        }
    }

    async fetchCreate(email: string, name: string, group: string): Promise<void> {
        try{
            const formData = new FormData()
            formData.append('Nombre', name)
            formData.append('Email', email)
            formData.append('Grupo', group)
            const respuesta = await fetch (`${api}`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: name,
                    email: email,
                    group: group
                })
            })
            if(respuesta.ok){
                alert("Usuario registrado")
                window.location.href = 'estudiantes'
            }else{
                console.log(respuesta.status)
            }
        }catch(error){
            console.error(error)
        }
    }

    async deleteStudent(id:number): Promise<void> {
        try{
            const respuesta = await fetch(api + '/' + id,{
                method: 'DELETE',
                headers:{
                    'Content-Type': 'application/json'
                },
            })
            const data = await respuesta.json()
            console.log(data)
        }catch(error){
            console.log(error)
        }
    }

}